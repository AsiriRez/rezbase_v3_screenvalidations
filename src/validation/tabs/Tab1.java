package validation.tabs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tab1 {

	private WebDriver driver;
	private FirefoxProfile prof;
	private String UserName = "Madhushani";
	private String PassWord = "123456";
	private StringBuffer PrintWriter;
	public static int TestCaseCount;
	private Map<String, String> Propertymap;
	StopWatch st = new StopWatch();

	@Before
	public void setUp() {
		if (PG_Properties.getProperty("Driver.Type").equals("Firefox")) {
			if (PG_Properties.getProperty("Profile.Use").equals("true"))
				prof = new FirefoxProfile(new File(
						PG_Properties.getProperty("Profile.Path")));
			else
				prof = new FirefoxProfile();

			driver = new FirefoxDriver(prof);
		} else {
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					PG_Properties.getProperty("Phantom.Bin"));
			driver = new PhantomJSDriver(cap);
		}
		PrintWriter = new StringBuffer();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void runTest() throws WebDriverException, IOException {

		PrintWriter
				.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter
				.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Screens Validation Script </p></div>");
		PrintWriter.append("<body>");
		TestCaseCount = 1;
		// Tab1 Flow = new Tab1(Propertymap, PrintWriter);

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		PrintWriter.append("<br><br>");
		PrintWriter
				.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
		PrintWriter
				.append("<p class='InfoSub' style='font-weight: bold;'>Login Info</p>");
		PrintWriter.append("<p class='InfoSub'>Server URL: "
				+ PG_Properties.getProperty("Portal.Url") + "</p>");
		PrintWriter.append("<p class='InfoSub'>Date: "
				+ dateFormat.format(date) + "</p>");

		PrintWriter.append("</div>");
		PrintWriter.append("<br>");

		PrintWriter
				.append("<p class='InfoSub' style='font-weight: bold;'>Basic Scenario validation</p>");
		PrintWriter
				.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
		PrintWriter.append("<tr><td>" + TestCaseCount
				+ "</td><td>Check Whether Logged to the System</td>");

		Tab1.TestCaseCount++;
		PrintWriter.append("<td>Log in Succussfully</td>");

		WebDriverWait wait = new WebDriverWait(driver, 100);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		try {

			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

			try {
				driver.get(PG_Properties.getProperty("Portal.Url")
						+ "/admin/common/LoginPage.do");
				driver.findElement(By.id("user_id")).clear();
				driver.findElement(By.id("user_id")).sendKeys(UserName);
				driver.findElement(By.id("password")).clear();
				driver.findElement(By.id("password")).sendKeys(PassWord);
				Thread.sleep(500);
				driver.findElement(By.id("loginbutton")).click();

				PrintWriter.append("<td>Log in Succussfully</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Login" + ".jpg"));
			} catch (Exception e) {
				PrintWriter.append("<td>Log in Failed</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Login" + ".jpg"));
			}
			PrintWriter.append("</tr></table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'>Operation Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			TestCaseCount = 1;
			Tab1.TestCaseCount++;

			// Booking List

			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By
					.xpath(".//*[@id='main_mnu_0']"));
			action.moveToElement(we).build().perform();

			driver.findElement(By.id("mnu_5")).click();
			Thread.sleep(8000);
			System.out.println("Succussfully loaded booking list report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Booking List Report loaded Succussfully  </td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Booking List Report Succussfully loaded </td>");

			driver.switchTo().frame("reportIframe");
			String posLocation = driver.findElement(By.id("posLocationName"))
					.getAttribute("value");

			if (posLocation.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Booking List Report Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Booking List Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Booking List Report not loaded </td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Booking List Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();
			Actions action1 = new Actions(driver);
			WebElement we1 = driver.findElement(By
					.xpath(".//*[@id='main_mnu_0']"));
			action1.moveToElement(we1).build().perform();

			// Call Center Reservations
			driver.findElement(By.id("mnu_6")).click();
			Thread.sleep(10000);
			System.out.println("Succussfully loaded Call Center Reservations");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Call Center Reservation Screen loaded  </td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Call Center Reservation Screen Succussfully loaded </td>");

			driver.switchTo().frame("live_message_frame");

			try {
				driver.findElement(By.id("select_currency"));
				PrintWriter
						.append("<td>Call Center Reservation Screen Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Call Center Reservations" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Call Center Reservation Screen not loaded </td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");

				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Call Center Reservations" + ".jpg"));
			}

			// /////////////////

			driver.switchTo().defaultContent();
			Actions action2 = new Actions(driver);
			WebElement we2 = driver.findElement(By
					.xpath(".//*[@id='main_mnu_0']"));
			action2.moveToElement(we2).build().perform();

			driver.findElement(By.id("mnu_7")).click();
			Alert alert3 = driver.switchTo().alert();
			alert3.accept();

			Thread.sleep(8000);
			System.out.println("Succussfully loaded Offline booking");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Offline booking form loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Offline booking form Succussfully loaded</td>");

			driver.switchTo().frame("live_message_frame");
			String country = driver.findElement(By.id("H_cmbNoOfAdults_R1"))
					.getAttribute("value");

			if (country.equalsIgnoreCase("1")) {
				PrintWriter
						.append("<td>Offline booking form Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Offline booking form"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Offline booking form not loaded </td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Offline booking form"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Web

			/*Actions action3 = new Actions(driver);
			WebElement we3 = driver.findElement(By
					.xpath(".//*[@id='main_mnu_0']"));
			action3.moveToElement(we3).build().perform();
			driver.findElement(By.id("mnu_8")).click();

			Alert alert4 = driver.switchTo().alert();
			alert4.accept();
			Thread.sleep(8000);
			System.out.println("Succussfully loaded Web reservations");
			Thread.sleep(10000);*/
			
			driver.get(PG_Properties.getProperty("Portal.Url.WebReservation"));
			Alert alert4 = driver.switchTo().alert();
			alert4.accept();
			Thread.sleep(10000);
			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Web Reservation Loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Web Reservation Screen Succussfully Loaded</td>");

			driver.switchTo().frame("bec_container_frame");

			try {
				driver.findElement(By.id("H_Country"));
				PrintWriter
						.append("<td>Web Reservation Screen Succussfully Loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Web Reservation Screen" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Web Reservation not Loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Web Reservation Screen" + ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Alert alert9 = driver.switchTo().alert(); alert9.accept();
			Thread.sleep(100);

			// ////////////// Modification
			/*
			 * driver.findElement(By.xpath(".//*[@id='main_mnu_1']")); Actions
			 * action7 = new Actions(driver); WebElement we7 =
			 * driver.findElement(By.xpath(".//*[@id='mnu_5']"));
			 * action7.moveToElement(we7).build().perform();
			 */

			// ////////////
			/*
			 * JavascriptExecutor js1 = (JavascriptExecutor) driver;
			 * js1.executeScript("cmItemMouseUp (this,6)"); Thread.sleep(1000);
			 * Alert alert9 = driver.switchTo().alert(); alert9.accept();
			 */

			// //////////////////

			// Confirmation

			/*
			 * Actions action8 = new Actions(driver);
			 * driver.findElement(By.xpath(".//*[@id='main_mnu_2']"));
			 * WebElement we8 =
			 * driver.findElement(By.xpath(".//*[@id='mnu_5']"));
			 * action8.moveToElement(we8).build().perform();
			 */

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/operations/reservation/ModificationCancellationInfoPage.do?module=operations");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Modification Screen Loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Booking Modification Screen Succussfully Loaded </td>");

			try {

				driver.findElement(By.id("reservationId"));
				PrintWriter
						.append("<td>Booking Modification Screen Succussfully Loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Booking Modification Screen" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Booking Modification Screen not Loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Booking Modification Screen" + ".jpg"));
			}

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=33&reportName=Booking%20Confirmation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Booking Confirmation Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Booking Confirmation Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String dateYr = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (dateYr.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Booking Confirmation Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Booking Confirmation Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Booking Confirmation Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Booking Confirmation Report" + ".jpg"));
			}

			driver.switchTo().defaultContent();
			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=27&reportName=Quotation%20Report");

			// Reports and Alerts

			// AirlinePassengerReport

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/air/AirlinePassengerReport.do?module=operations");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check AirlinePassenger Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>AirlinePassenger Report Succussfully loaded</td>");

			String dateFromYr = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (dateFromYr.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>AirlinePassenger Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "AirlinePassengerReport" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>AirlinePassenger Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "AirlinePassengerReport" + ".jpg"));
			}

			// Auto Cancellation Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=25&reportName=Auto%20Cancellation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Auto Cancellation Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Auto Cancellation Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String hotelName = driver.findElement(By.id("name")).getAttribute(
					"value");
			if (hotelName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Auto Cancellation Report Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Auto Cancellation Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Auto Cancellation Report not loaded </td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Auto Cancellation Report" + ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Booking Modification Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=40&reportName=Booking%20Modification%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Booking Modification Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Booking Modification Report Succussfully loaded </td>");

			driver.switchTo().frame("reportIframe");
			String cityInventory = driver.findElement(By.id("city_inventory"))
					.getAttribute("value");

			if (cityInventory.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Booking Modification Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Booking Modification Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Booking Modification Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Booking Modification Report" + ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Cancellation Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=1&reportName=Cancellation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Cancellation Report Loaded Sucussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Cancellation Report Succussfully Loaded</td>");

			driver.switchTo().frame("reportIframe");
			String supplier = driver.findElement(By.id("supplierName_C"))
					.getAttribute("value");
			if (supplier.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Cancellation Report Succussfully Loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Cancellation Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Cancellation Report not Loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Cancellation Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();
			// Customer Chaser Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=43&reportName=Customer%20Chaser%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check whether Customer Chaser Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Customer Chaser Report Succussfully loaded </td>");

			driver.switchTo().frame("reportIframe");
			String datef = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (datef.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Customer Chaser Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Customer Chaser Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Customer Chaser Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Customer Chaser Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Failed Booking Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=45&reportName=Failed%20Booking%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check whether Failed Booking Report Loaded Succussfully </td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Failed Booking Report Succussfully Loaded</td>");

			driver.switchTo().frame("reportIframe");
			String yrId = driver.findElement(By.id("reportto_Year_ID"))
					.getAttribute("value");
			if (yrId.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Failed Booking Report Succussfully Loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Failed Booking Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Failed Booking Report not Loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Failed Booking Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Notes Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/HotelNoteReport.do?module=operations");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Notes Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Notes Report Succussfully loaded</td>");

			String month = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (month.equalsIgnoreCase("2015")) {
				PrintWriter.append("<td>Notes Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Notes Report"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Notes Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Notes Report"
										+ ".jpg"));
			}

			// Queued Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=46&reportName=Queued%20Report");
			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Queued Report succussfully loaded</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Queued Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			try {

				driver.findElement(By.id("searchby_label_text"));
				PrintWriter
						.append("<td>Queued Report succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Queued Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Queued Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Queued Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Reservation Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=31&reportName=Reservation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Reservation Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Reservation Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String pos = driver.findElement(By.id("toPosLocationName"))
					.getAttribute("value");
			if (pos.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Reservation Report  Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Reservation Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Reservation Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Reservation Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Rooming List Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=22&reportName=Rooming%20List%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Rooming List Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Rooming List Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String city = driver.findElement(By.id("city_inventory"))
					.getAttribute("value");
			if (city.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Rooming List Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Rooming List Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Rooming List Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Rooming List Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();
			
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'>Contracting Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			
			Tab1.TestCaseCount++;

			// Contracting

			// FlightCommissionaReport

			driver.get(PG_Properties.getProperty("Portal.Url.Air")
					+ "/FlightCommissionaReport.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Flight Commission Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Flight Commission Report Succussfully loaded</td>");

			String sup = driver.findElement(By.id("airlinesupplier"))
					.getAttribute("value");
			if (sup.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Flight Commission Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Flight Commissional Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Flight Commission Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Flight Commissional Report" + ".jpg"));
			}

			// AirConfigurationSetup

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/air/setup/AirConfigurationSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Air Configuration Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Air Configuration Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("airLineName"));
				PrintWriter
						.append("<td>Air Configuration Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Air Configuration Setup" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Air Configuration Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Air Configuration Setup" + ".jpg"));
			}

			// AirBookingFee

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/air/setup/AirBookingFeeSetupPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check AirBookingFee loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>AirBookingFee Succussfully loaded</td>");

			String region = driver.findElement(By.id("regionName"))
					.getAttribute("value");
			if (region.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>AirBookingFee Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "AirBookingFee"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>AirBookingFee not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "AirBookingFee"
								+ ".jpg"));
			}

			// Commission Setup

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/air/setup/CommissionSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Commission Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Commission Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Commission Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Commission Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Commission Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Commission Setup"
								+ ".jpg"));
			}

			// Assign Alternative Availability

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/AssignAlternativeAvailabilityPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Alternative Availability loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Alternative Availability Succussfully loaded</td>");

			String Suburb = driver.findElement(By.id("suburb")).getAttribute(
					"value");
			if (Suburb.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Alternative Availability Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Assign Alternative Availability" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Alternative Availability not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Assign Alternative Availability" + ".jpg"));
			}

			// Availability & Rates � By Period

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/InventorySetupPeriodBasisPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Availability & Rates By Period loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Availability & Rates By Period Succussfully loaded</td>");

			try {
				driver.findElement(By.id("periodFrom_Year_ID"));
				PrintWriter
						.append("<td>Availability & Rates By Period Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Availability & Rates By Period" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Availability & Rates By Period not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Availability & Rates By Period" + ".jpg"));
			}

			// Selected Dates Basis

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/InventoryRatesDatesBasisPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Selected Dates Basis loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Selected Dates Basis Succussfully loaded</td>");

			try {
				driver.findElement(By.id("toRegionName"));
				PrintWriter
						.append("<td>Selected Dates Basis Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Selected Dates Basis"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Selected Dates Basis not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Selected Dates Basis"
								+ ".jpg"));
			}

			// Availability & Rates � Modify Inventory & Rates

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/InventoryRatesSelectedDatesPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Availability & Rates  Modify Inventory & Rates not loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Availability & Rates  Modify Inventory & Rates Succussfully loaded</td>");

			String toregion = driver.findElement(By.id("toRegionName"))
					.getAttribute("value");
			if (toregion.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Availability & Rates  Modify Inventory & Rates Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Modify Inventory & Rates" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Availability & Rates  Modify Inventory & Rates not not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Modify Inventory & Rates" + ".jpg"));
			}

			// Availability & Rates � Block Inventory

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/BlockInventoryPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Block Inventory loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Block Inventory Succussfully loaded</td>");

			try {
				driver.findElement(By.id("hotelName"));
				PrintWriter
						.append("<td>Block Inventory Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Block Inventory"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Block Inventory not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Block Inventory"
								+ ".jpg"));
			}

			// Setup Bed Types

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/BedTypePage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Bed Types setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Bed Types setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("bedTypeName"));
				PrintWriter
						.append("<td>Bed Types setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Setup Bed Types"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Bed Types setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Setup Bed Types"
								+ ".jpg"));
			}

			// Lookup Info � Hotel Group

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/HotelGroupPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Hotel Group loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Hotel Group Succussfully loaded</td>");

			try {
				driver.findElement(By.id("hotelgroupName"));
				PrintWriter.append("<td>Hotel Group Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Hotel Group"
										+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Hotel Group not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Hotel Group"
										+ ".jpg"));
			}

			// Lookup Info � Rate Plan

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/SetupRatePlanPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Rate Plan loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Rate Plan Succussfully loaded</td>");

			try {
				driver.findElement(By.id("ratePlanName"));
				PrintWriter.append("<td>Rate Plan Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Rate Plan" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Rate Plan not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Rate Plan" + ".jpg"));
			}

			// Room Type Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/RoomTypeDetailsPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Room Type Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Room Type Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("roomTypeName"));
				PrintWriter
						.append("<td>Room Type Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Room Type Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Room Type Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Room Type Setup"
								+ ".jpg"));
			}

			// Lookup Info � Star Category

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/SetupStarCategoryPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Star Category loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Star Category Succussfully loaded </td>");

			try {
				driver.findElement(By.id("starCategoryName"));
				PrintWriter
						.append("<td>Star Category Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Star Category"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Star Category not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Star Category"
								+ ".jpg"));
			}

			// Assign Room Occupancy

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/AssignRoomOccupancyPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Room Occupancy loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Room Occupancy Succussfully loaded</td>");

			try {
				driver.findElement(By.id("hotelname"));
				PrintWriter
						.append("<td>Room Occupancy Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Assign Room Occupancy"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Room Occupancy not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Assign Room Occupancy"
								+ ".jpg"));
			}

			// Hotel Setup � Hotel Info

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/HotelSetupStandardPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Hotel Info loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Hotel Info Succussfully loaded</td>");

			try {
				driver.findElement(By.id("productCode"));
				PrintWriter.append("<td>Hotel Info Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Hotel Info" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Hotel Info not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Hotel Info" + ".jpg"));
			}

			// Lookup Info � Supplementary Services

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/setup/SupplementaryServicesPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplementary Services loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplementary Services Succussfully loaded</td>");

			String tour = driver.findElement(By.id("touroperatorname"))
					.getAttribute("value");
			if (tour.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Supplementary Services Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Supplementary Services" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Supplementary Services not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Supplementary Services" + ".jpg"));
			}

			// Hotel Booking Fee

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/admin/setup/BookingFeeSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Booking Fee setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Booking Fee setup Succussfully loaded</td>");

			String state = driver.findElement(By.id("stateName")).getAttribute(
					"value");
			if (state.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Booking Fee setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Hotel Booking Fee"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Booking Fee setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Hotel Booking Fee"
								+ ".jpg"));
			}

			// Profit Markup � By Period

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/ProfitMarkupPeriodBasisPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit Markup By Period loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit Markup By Period Succussfully loaded</td>");

			try {
				driver.findElement(By.id("rateRegionName"));
				PrintWriter
						.append("<td>LProfit Markup By Period Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Profit Markup By Period" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Profit Markup By Period not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Profit Markup By Period" + ".jpg"));
			}

			// Profit Markup � Modify Profit Markup

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/ProfitMarkupSelectedDatesPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit Markup Modify Profit Markup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit Markup Modify Profit Markup Succussfully loaded</td>");

			String rateRegionName = driver.findElement(By.id("rateRegionName"))
					.getAttribute("value");
			if (rateRegionName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Profit Markup Modify Profit Markup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Modify Profit Markup"
								+ ".jpg"));

			} else {
				PrintWriter
						.append("<td>Profit Markup Modify Profit Markup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Modify Profit Markup"
								+ ".jpg"));
			}

			// Highlight Products - Hotel

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/BestRateDealOfTheDayPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Highlight Products loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Highlight Products Succussfully loaded</td>");

			try {
				driver.findElement(By.id("countryName"));
				PrintWriter
						.append("<td>Highlight Products Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Highlight Products"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Highlight Products not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Highlight Products"
								+ ".jpg"));
			}

			// Hotels Special

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/HotelsSpecialPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Hotels Special loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Hotels Special Succussfully loaded</td>");

			try {
				driver.findElement(By.id("hotelName"));
				PrintWriter
						.append("<td>Hotels Special Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Hotels Special"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Hotels Special not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Hotels Special"
								+ ".jpg"));
			}

			// Promotion � Special Promotion

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/SpecialPromotionPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Special Promotion loaded sucussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Special Promotion Succussfully loaded</td>");

			String roomType = driver.findElement(By.id("roomType"))
					.getAttribute("value");
			if (roomType.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Special Promotion Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Special Promotion"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Special Promotion not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Special Promotion"
								+ ".jpg"));
			}

			// Discount � Standard Discount

			driver.get(PG_Properties.getProperty("Portal.Url.Hotels")
					+ "/inventoryandrates/StandardDiscountPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Standard Discount loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Standard Discount Succussfully loaded</td>");

			String cityName = driver.findElement(By.id("cityName"))
					.getAttribute("value");
			if (cityName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Standard Discount Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Standard Discount"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Standard Discount not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Standard Discount"
								+ ".jpg"));
			}

			// Arrival Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=14&reportName=Arrival%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Arrival Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Arrival Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String hotel = driver.findElement(By.id("hotelname")).getAttribute(
					"value");
			if (hotel.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Arrival Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Arrival Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Arrival Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Arrival Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Blackout Report
			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=15&reportName=Blackout%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Blackout Reportt loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Blackout Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.id("touroperatorname"));
				PrintWriter
						.append("<td>Blackout Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Blackout Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Blackout Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Blackout Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Commission Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=16&reportName=Commission%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Commission Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Commission Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String hName = driver.findElement(By.id("hotelname")).getAttribute(
					"value");
			if (hName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Commission Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Commission Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Commission Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Commission Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Contract Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=41&reportName=Contract%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Contract Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Contract Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String htName = driver.findElement(By.id("hotelname"))
					.getAttribute("value");
			if (htName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Contract Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Contract Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Contract Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Contract Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Customer Fax Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/hotels/OpsCustomerFaxReport.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Customer Fax Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Customer Fax Report Succussfully loaded</td>");

			String supp = driver.findElement(By.id("suppliername"))
					.getAttribute("value");
			if (supp.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Customer Fax Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Customer Fax Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Customer Fax Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Customer Fax Report"
								+ ".jpg"));
			}

			// Discount Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=17&reportName=Discount%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Discount Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Discount Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String supplierN = driver.findElement(By.id("supplierName"))
					.getAttribute("value");
			if (supplierN.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Discount Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Discount Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Discount Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Discount Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Hotel Details Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=18&reportName=Hotel%20Details%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Hotel Details Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Hotel Details Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String starCategoryName = driver.findElement(
					By.id("starCategoryName")).getAttribute("value");
			if (starCategoryName.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Hotel Details Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Hotel Details Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Hotel Details Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Hotel Details Report"
								+ ".jpg"));
			}

			// Inventory Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=19&reportName=Inventory%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Inventory Report Succussfully loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Inventory Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.id("touroperatorname"));
				PrintWriter
						.append("<td>Inventory Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Inventory Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Inventory Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Inventory Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Profit Markup Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=20&reportName=Profit%20Markup%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit Markup Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit Markup Report loaded Succussfully</td>");

			driver.switchTo().frame("reportIframe");
			String bedType = driver.findElement(By.id("roomtypebedtype"))
					.getAttribute("value");
			if (bedType.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Profit Markup Report loaded Succussfully</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Profit Markup Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Profit Markup Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Profit Markup Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Rates Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=21&reportName=Rates%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Rates Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Rates Report loaded Succussfully</td>");

			driver.switchTo().frame("reportIframe");
			String rmType = driver.findElement(By.id("roomType")).getAttribute(
					"value");
			if (rmType.equalsIgnoreCase("ALL")) {
				PrintWriter.append("<td>Rates Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Rates Report"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Rates Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Rates Report"
										+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Supplementary Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=23&reportName=Supplementary%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplementary Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplementary Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String hNme = driver.findElement(By.id("hotelname")).getAttribute(
					"value");
			if (hNme.equalsIgnoreCase("ALL"))
				try {
					PrintWriter
							.append("<td>Supplementary Report Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("PassedScreenshots/"
									+ "Supplementary Report" + ".jpg"));

				} catch (Exception e) {
					PrintWriter
							.append("<td>Supplementary Report not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("FailedScreenshots/"
									+ "Supplementary Report" + ".jpg"));
				}
			driver.switchTo().defaultContent();

			// Activity Program Setup � Activity Program Info

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/ProgramSetupStandardPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Activity Program Info loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Activity Program Info Succussfully loaded</td>");

			String pax = driver.findElement(By.id("minimumPax")).getAttribute(
					"value");
			if (pax.equalsIgnoreCase("0"))
				try {
					PrintWriter
							.append("<td>Activity Program Info Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("PassedScreenshots/"
									+ "Activity Program Info" + ".jpg"));

				} catch (Exception e) {
					PrintWriter
							.append("<td>Activity Program Info not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("FailedScreenshots/"
									+ "Activity Program Info" + ".jpg"));
				}

			// Assign Activity Period

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/AssignProgramActivityPeriodPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Activity Period loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Activity Period Succussfully loaded</td>");

			try {
				driver.findElement(By.id("programname"));
				PrintWriter
						.append("<td>Activity Period Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ " Assign Activity Period" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Activity Period not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ " Assign Activity Period" + ".jpg"));
			}

			// Availability & Rates � By Period

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/InventorySetupPeriodBasisPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Availability & Rates By Period loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Availability & Rates By Period Succussfully loaded</td>");

			try {
				driver.findElement(By.id("customerName"));
				PrintWriter
						.append("<td>Availability & Rates By Period Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Availability & Rates By Period" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Availability & Rates By Period not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Availability & Rates By Period" + ".jpg"));
			}

			// Selected Dates Basis

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/InventoryRatesDatesBasisPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Dates Basis loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Dates Basis Succussfully loaded</td>");

			try {
				driver.findElement(By.id("customerName"));
				PrintWriter.append("<td>Dates Basis Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Selected Dates Basis"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Dates Basis not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Selected Dates Basis"
								+ ".jpg"));
			}

			// Availability & Rates � Modify Inventory and Rates

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/InventoryRatesSelectedDatesPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Modify Inventory and Rates loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Modify Inventory and Rates Succussfully loaded</td>");

			try {
				driver.findElement(By.id("toRegionName"));
				PrintWriter
						.append("<td>Modify Inventory and Rates Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Modify Inventory and Rates" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Modify Inventory and Rates not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Modify Inventory and Rates" + ".jpg"));
			}

			// Availability & Rates � Block Inventory

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/BlockInventoryPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Block Inventory loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Block Inventory Succussfully loaded</td>");

			String block = driver.findElement(By.id("toRegionName"))
					.getAttribute("value");
			if (block.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Block Inventory Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Block Inventory"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Block Inventory not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Block Inventory"
								+ ".jpg"));
			}

			// Lookup Info � Program Type Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/ProgramTypePage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Program Type Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td> Program Type Setup Succussfully loaded</td>");

			String rank = driver.findElement(By.id("rank")).getAttribute(
					"value");
			if (rank.equalsIgnoreCase("0")) {
				PrintWriter
						.append("<td> Program Type Setup loaded succussfullyy</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Program Type Setup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td> Program Type Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Program Type Setup"
								+ ".jpg"));
			}

			// Lookup Info � Activity Type Setup
			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/ActivityTypeDetailsPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Activity Type Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Activity Type Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("activityTypeName"));
				PrintWriter
						.append("<td>Activity Type Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Activity Type Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Activity Type Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Activity Type Setup"
								+ ".jpg"));
			}

			// Lookup Info � NearBy Attractions
			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/NearByAttractionsPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check NearBy Attractions loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td> NearBy Attractions Succussfully loaded</td>");

			try {
				driver.findElement(By.id("nearByAttractionsName"));
				PrintWriter
						.append("<td> NearBy Attractions Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "NearBy Attractions"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td> NearBy Attractions not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "NearBy Attractions"
								+ ".jpg"));
			}

			// Lookup Info � Period Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/PeriodPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Period Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Period Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("periodDescription"));
				PrintWriter.append("<td>Period Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Lookup Info Period Setup" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Period Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Lookup Info Period Setup" + ".jpg"));
			}

			// Lookup Info � Rate Plan
			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/SetupRatePlanPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Rate Plan loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Rate Plan Succussfully loaded</td>");

			try {
				driver.findElement(By.id("minPersons"));
				PrintWriter.append("<td>Rate Plan Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Rate Plan" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Rate Plan not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Rate Plan" + ".jpg"));
			}

			// Profit Markup � By Period

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/ProfitMarkupPeriodBasisPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit Markup � By Period loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit Markup � By Period Succussfully loaded</td>");

			String rate = driver.findElement(By.id("rateRegionName"))
					.getAttribute("value");
			if (rate.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Profit Markup � By Period Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Profit Markup By Period" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Profit Markup � By Period not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Profit Markup By Period" + ".jpg"));
			}

			// Profit Markup � Modify Profit Markup

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/inventoryandrates/ProfitMarkupSelectedDatesPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Modify Profit Markup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Modify Profit Markup Succussfully loaded</td>");

			String rateR = driver.findElement(By.id("rateRegionName"))
					.getAttribute("value");
			if (rateR.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Modify Profit Markup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Modify Profit Markup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Modify Profit Markup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Modify Profit Markup"
								+ ".jpg"));
			}

			// Arrival Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=5&reportName=Arrival%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Arrival Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Arrival Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String prog = driver.findElement(By.id("programname"))
					.getAttribute("value");
			if (prog.equalsIgnoreCase("ALL"))
				try {
					PrintWriter
							.append("<td>Arrival Report Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("PassedScreenshots/" + "Arrival Report"
									+ ".jpg"));

				} catch (Exception e) {
					PrintWriter.append("<td>Arrival Report not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("FailedScreenshots/" + "Arrival Report"
									+ ".jpg"));
				}

			driver.switchTo().defaultContent();

			// Blackout Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=9&reportName=Blackout%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Blackout Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td> Blackout Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.id("customerName"));
				PrintWriter
						.append("<td> Blackout Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Blackout Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td> Blackout Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Blackout Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Commission Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=10&reportName=Commission%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Commission Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Commission Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String comm = driver.findElement(By.id("programname"))
					.getAttribute("value");
			if (comm.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Commission Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Commission Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Commission Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Commission Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Inventory Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=11&reportName=Inventory%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Inventory Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Inventory Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			try {
				driver.findElement(By.id("customerName"));
				PrintWriter
						.append("<td>Inventory Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Inventory Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Inventory Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Inventory Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Profit Markup Report
			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=12&reportName=Profit%20Markup%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit Markup Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit Markup Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String pm = driver.findElement(By.id("activitytypeperiodtype"))
					.getAttribute("value");
			if (pm.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Profit Markup Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Profit Markup Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Profit Markup Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Profit Markup Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Rates Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=13&reportName=Rates%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Rates Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Rates Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String pogram = driver.findElement(By.id("programname"))
					.getAttribute("value");
			if (pogram.equalsIgnoreCase("ALL")) {
				PrintWriter.append("<td>Rates Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Rates Report"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Rates Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Rates Report"
										+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Car

			// Car Commission Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/car/CarCommissionReport.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Car Commission Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Car Commission Report Succussfully loaded</td>");

			String suplier = driver.findElement(By.id("supplierName"))
					.getAttribute("value");
			if (suplier.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Car Commission Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Car Commission Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Car Commission Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Car Commission Report"
								+ ".jpg"));
			}

			// Car Rental Profit Markup Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/car/CarRentalProfitMarkupReport.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Car Rental Profit Markup Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Car Rental Profit Markup Report Succussfully loaded</td>");

			String car = driver.findElement(By.id("carTypeName")).getAttribute(
					"value");
			if (car.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Car Rental Profit Markup Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Car Rental Profit Markup Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Car Rental Profit Markup Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Car Rental Profit Markup Report" + ".jpg"));
			}

			// Car Configurations Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Car")
					+ "/setup/CarConfigurationSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Car Configurations Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Car Configurations Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("carSupplierName"));
				PrintWriter
						.append("<td>Car Configurations Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Car Configurations Setup" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Car Configurations Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Car Configurations Setup" + ".jpg"));
			}

			// Car Booking Fee Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Car")
					+ "/setup/CarBookingFeeSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Car Booking Fee Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Car Booking Fee Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("carTypeName"));
				PrintWriter
						.append("<td>Car Booking Fee Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Car Booking Fee Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Car Booking Fee Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Car Booking Fee Setup"
								+ ".jpg"));
			}

			// Commission Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Car")
					+ "/setup/CommissionSetup.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Commission Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Commission Setup Succussfully loaded</td>");

			String cit = driver.findElement(By.id("cityName")).getAttribute(
					"value");
			if (cit.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Commission Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Commission Setup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Commission Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Commission Setup"
								+ ".jpg"));
			}

			// Fix Package

			// Fixed Package Setup

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/packaging/setup/PackageSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Fixed Package Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Fixed Package Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("packageName"));
				PrintWriter
						.append("<td>Fixed Package Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Fixed Package Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Fixed Package Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Fixed Package Setup"
								+ ".jpg"));
			}

			// Suppler

			// Product Materialization Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=37&reportName=Product%20Materialization%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Product Materialization Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Product Materialization Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String hnme = driver.findElement(By.id("hotelname")).getAttribute(
					"value");
			if (hnme.equalsIgnoreCase("ALL"))
				try {
					PrintWriter
							.append("<td>Product Materialization Report Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils
							.copyFile(screen.getScreenshotAs(OutputType.FILE),
									new File("PassedScreenshots/"
											+ "Product Materialization Report"
											+ ".jpg"));

				} catch (Exception e) {
					PrintWriter
							.append("<td>Product Materialization Report not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils
							.copyFile(screen.getScreenshotAs(OutputType.FILE),
									new File("FailedScreenshots/"
											+ "Product Materialization Report"
											+ ".jpg"));
				}

			driver.switchTo().defaultContent();

			// Supplier Details Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=28&reportName=Supplier%20Details%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplier Details Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplier Details Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String cityNme = driver.findElement(By.id("city")).getAttribute(
					"value");
			if (cityNme.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Supplier Details Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Supplier Details Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Supplier Details Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Supplier Details Report" + ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Supplier Setup � Supplier Info

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/SupplierSetupStandardPage.do?module=contract");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Supplier Info loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Supplier Info Succussfully loaded</td>");

			try {
				driver.findElement(By.id("addressline1"));
				PrintWriter
						.append("<td>Supplier Info Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Supplier Info"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Supplier Info not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Supplier Info"
								+ ".jpg"));
			}

			// Third Party Supplier Profit Markup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/RideSupplierProfitMarkupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Third Party Supplier Profit Markup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Third Party Supplier Profit Markup Succussfully loaded</td>");

			String regionname = driver.findElement(By.id("regionname"))
					.getAttribute("value");
			if (regionname.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Third Party Supplier Profit Markup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/"
										+ "Third Party Supplier Profit Markup"
										+ ".jpg"));

			} else {
				PrintWriter
						.append("<td>Third Party Supplier Profit Markup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/"
										+ "Third Party Supplier Profit Markup"
										+ ".jpg"));
			}

			// Star Category Overriding Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/StarCategoryOverridingSetupPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Star Category Overriding Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Star Category Overriding Setup Succussfully loaded</td>");

			String cName = driver.findElement(By.id("countryName"))
					.getAttribute("value");
			if (cName.equalsIgnoreCase("ALL"))
				try {
					PrintWriter
							.append("<td>Star Category Overriding Setup Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils
							.copyFile(screen.getScreenshotAs(OutputType.FILE),
									new File("PassedScreenshots/"
											+ "Star Category Overriding Setup"
											+ ".jpg"));

				} catch (Exception e) {
					PrintWriter
							.append("<td>Star Category Overriding Setup not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils
							.copyFile(screen.getScreenshotAs(OutputType.FILE),
									new File("FailedScreenshots/"
											+ "Star Category Overriding Setup"
											+ ".jpg"));
				}

			// Third Party Supplier to City Mappings

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/RideSupplierCityMappingPage.do?module=contract");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Third Party Supplier to City Mappings loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Third Party Supplier to City Mappings Succussfully loaded</td>");

			try {
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Third Party Supplier to City Mappings Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Third Party Supplier to City Mappings"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Third Party Supplier to City Mappings not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Third Party Supplier to City Mappings"
								+ ".jpg"));
			}
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'> Finance Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			
			Tab1.TestCaseCount++;

			// Finance

			// Invoice Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=6&reportName=Invoice%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Invoice Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Invoice Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String client = driver.findElement(By.id("customername"))
					.getAttribute("value");
			if (client.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Invoice Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Invoice Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Invoice Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Invoice Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Passenger Traveled Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/management/MgtPassengerTravelledReport.do?module=finance");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Passenger Traveled Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Passenger Traveled Report Succussfully loaded</td>");

			String yr = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (yr.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Passenger Traveled Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Passenger Traveled Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Passenger Traveled Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Passenger Traveled Report" + ".jpg"));
			}

			// Payment Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/operational/mainReport.do?reportId=8&reportName=Payment%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Payment Report  loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Payment Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String tourOp = driver.findElement(By.id("touroperatornameBulk"))
					.getAttribute("value");
			if (tourOp.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Payment Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Payment Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Payment Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Payment Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Credit Card Details Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=7&reportName=Credit%20Card%20Details%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Credit Card Details Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Credit Card Details Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String cop = driver.findElement(By.id("corporateCustomerName"))
					.getAttribute("value");
			if (cop.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Credit Card Details Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Credit Card Details Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Credit Card Details Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Credit Card Details Report" + ".jpg"));
			}

			// Prepay Supplier Report

			driver.get(PG_Properties
					.getProperty("Portal.Url.Operation.Reports")
					+ "/accounting/PrepaySupplierReport.do?module=finance");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Prepay Supplier Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Prepay Supplier Report Succussfully loaded</td>");

			try {
				driver.findElement(By.id("reportto_Month_ID"));
				PrintWriter
						.append("<td>Prepay Supplier Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Prepay Supplier Report" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Prepay Supplier Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Prepay Supplier Report" + ".jpg"));
			}

			// Supplier Payable Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=35&reportName=Supplier%20Payable%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplier Payable Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplier Payable Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String yearrId = driver.findElement(By.id("reportto_Year_ID"))
					.getAttribute("value");
			if (yearrId.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Supplier Payable Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Supplier Payable Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Supplier Payable Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Supplier Payable Report" + ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Third Party Supplier Payable Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=36&reportName=Third%20Party%20Supplier%20Payable%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Third Party Supplier Payable Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Third Party Supplier Payable Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String fmId = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (fmId.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Third Party Supplier Payable Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Third Party Supplier Payable Report"
								+ ".jpg"));

			} else {
				PrintWriter
						.append("<td>Third Party Supplier Payable Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Third Party Supplier Payable Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Credit Limit Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=3&reportName=Credit%20Limit%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Credit Limit Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Credit Limit Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String to = driver.findElement(By.id("touroperatorname"))
					.getAttribute("value");
			if (to.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Credit Limit Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Credit Limit Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Credit Limit Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Credit Limit Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Currency Exchange Rate Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=42&reportName=Currency%20Exchange%20Rate%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Currency Exchange Rate Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Currency Exchange Rate Report Succussfully loaded</td>");
			try {
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Currency Exchange Rate Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Currency Exchange Rate Report" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Currency Exchange Rate Report not</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Currency Exchange Rate Report" + ".jpg"));
			}

			// Profit and Loss Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=38&reportName=Profit%20and%20Loss%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit and Loss Report loaded Succussfully </td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit and Loss Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String stateName = driver.findElement(By.id("stateName"))
					.getAttribute("value");
			if (stateName.equalsIgnoreCase("ALL"))
				try {
					PrintWriter
							.append("<td>Profit and Loss Report Succussfully loaded</td>");
					PrintWriter.append("<td class='Passed'>PASS</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("PassedScreenshots/"
									+ "Profit and Loss Report" + ".jpg"));

				} catch (Exception e) {
					PrintWriter
							.append("<td>Profit and Loss Report not loaded</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td>");
					PrintWriter.append("<td></td></tr>");
					TakesScreenshot screen = (TakesScreenshot) driver;
					FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
							new File("FailedScreenshots/"
									+ "Profit and Loss Report" + ".jpg"));
				}
			driver.switchTo().defaultContent();

			// Sales Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=34&reportName=Sales%20Report");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Sales Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Sales Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String documentno = driver.findElement(By.id("documentno"))
					.getAttribute("value");
			if (documentno.equalsIgnoreCase("ALL")) {
				PrintWriter.append("<td>Sales Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Sales Report"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Sales Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Sales Report"
										+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// SupplierChaserReport

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=47&reportName=SupplierChaserReport");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplier Chaser Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplier Chaser Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String sub = driver.findElement(By.id("supplier_Name"))
					.getAttribute("value");
			if (sub.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Supplier Chaser Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Supplier Chaser Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Supplier Chaser Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Supplier Chaser Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'>Management Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			
			Tab1.TestCaseCount++;
			 
			// Management

			// Aging Analysis Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=2&reportName=Aging%20Analysis%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Aging Analysis Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Aging Analysis Report Succussfully loaded </td>");

			driver.switchTo().frame("reportIframe");
			try {

				driver.findElement(By.id("excelCreation_N"));
				PrintWriter
						.append("<td>Aging Analysis Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Aging Analysis Report"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Aging Analysis Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Aging Analysis Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Cancellation Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=1&reportName=Cancellation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Cancellation Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Cancellation Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String supply = driver.findElement(By.id("supplierName_C"))
					.getAttribute("value");
			if (supply.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Cancellation Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Cancellation Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Cancellation Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Cancellation Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Profit and Loss Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=38&reportName=Profit%20and%20Loss%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Profit and Loss Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Profit and Loss Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String stat = driver.findElement(By.id("stateName")).getAttribute(
					"value");
			if (stat.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Profit and Loss Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Profit and Loss Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Profit and Loss Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Profit and Loss Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Reservation Report

			driver.get(PG_Properties.getProperty("Portal.Url.Operation")
					+ "/mainReport.do?reportId=31&reportName=Reservation%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Reservation Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Reservation Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String posLoc = driver.findElement(By.id("toPosLocationName"))
					.getAttribute("value");
			if (posLoc.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Reservation Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Reservation Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Reservation Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Reservation Report"
								+ ".jpg"));
			}

			driver.switchTo().defaultContent();

			// Usage Statistic Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=29&reportName=Usage%20Statistic%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Usage Statistic Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Usage Statistic Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String destination = driver.findElement(By.id("destination"))
					.getAttribute("value");
			if (destination.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Usage Statistic Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Usage Statistic Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Usage Statistic Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Usage Statistic Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// User Access Audit Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=30&reportName=User%20Access%20Audit%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check User Access Audit Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>User Access Audit Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String audit = driver.findElement(By.id("auditusername"))
					.getAttribute("value");
			if (audit.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>User Access Audit Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "User Access Audit Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>User Access Audit Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "User Access Audit Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Report Column Setup

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "Reports/admin/ReportColumnSetup.do");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Report Column Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Report Column Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Report Column Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Report Column Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Report Column Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Report Column Setup"
								+ ".jpg"));
			}

			// Report Filter Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Reports")
					+ "/admin/ReportFilterSetup.do");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Report Filter Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Report Filter Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("screenname"));
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Report Filter Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Report Filter Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Report Filter Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Report Filter Setup"
								+ ".jpg"));
			}

			// Report Setup
			driver.get(PG_Properties.getProperty("Portal.Url.Reports")
					+ "/admin/ReportSetup.do");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Report Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Report Setup Succussfully loaded</td>");

			String rep = driver.findElement(By.id("recordsPerPage"))
					.getAttribute("value");
			if (rep.equalsIgnoreCase("20")) {
				PrintWriter.append("<td>Report Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Report Setup"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Report Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Report Setup"
										+ ".jpg"));
			}

			// Report User Type Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Reports")
					+ "/admin/ReportUserTypeSetup.do");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Report User Type Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Report User Type Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("userTypeName"));
				PrintWriter
						.append("<td>Report User Type Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Report User Type Setup" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Report User Type Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Report User Type Setup" + ".jpg"));
			}
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'>Marketing Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			
			Tab1.TestCaseCount++;

			// Marketing

			// Assign Guide Programs

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/AssignGuideProgramsPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Assign Guide Programs loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Assign Guide Programs Succussfully loaded</td>");

			try {

				driver.findElement(By.id("programName"));
				PrintWriter
						.append("<td>Assign Guide Programs Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Assign Guide Programs"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Assign Guide Programs not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Assign Guide Programs"
								+ ".jpg"));
			}

			// Block Guide Dates

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/BlockGuideDatesPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Block Guide Dates loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Block Guide Dates Succussfully loaded</td>");

			try {
				driver.findElement(By.id("guideName"));
				PrintWriter
						.append("<td>Block Guide Dates Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Block Guide Dates"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Block Guide Dates not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Block Guide Dates"
								+ ".jpg"));
			}

			// Affiliate Commission Template Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/AffiliateCommissionTemplateSetupPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Commission Template Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Affiliate Commission Template Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("templateCode"));
				PrintWriter
						.append("<td>Affiliate Commission Template Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Affiliate Commission Template Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Affiliate Commission Template Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Affiliate Commission Template Setup"
								+ ".jpg"));
			}

			// Setup � B2B / Partner

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/RegionTourOperatorSetup.do?module=marketing");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check  B2B / Partner loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>B2B / Partner Succussfully loaded</td>");

			String creditLimit = driver.findElement(By.id("creditLimit"))
					.getAttribute("value");
			if (creditLimit.equalsIgnoreCase("0.00")) {

				PrintWriter
						.append("<td>B2B / Partner Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "B2B Partner"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>B2B / Partner not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "B2B  Partner"
										+ ".jpg"));
			}

			// Affiliate Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/AffiliateSetupStandardPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Affiliate Setup Succussfully loaded</td>");

			String yrTo = driver.findElement(By.id("periodTo_Year_ID"))
					.getAttribute("value");
			if (yrTo.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Affiliate Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Affiliate Setup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Affiliate Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Affiliate Setup"
								+ ".jpg"));
			}

			// Discount Coupon Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/SetupDiscountCouponPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Discount Coupon Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Discount Coupon Setup Succussfully loaded</td>");

			String cntry = driver.findElement(By.id("countryName"))
					.getAttribute("value");
			if (cntry.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Discount Coupon Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Discount Coupon Setup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Discount Coupon Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Discount Coupon Setup"
								+ ".jpg"));
			}

			// Guide Info

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/GuidePage.do?module=marketing");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Guide Info loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Guide Info Succussfully loaded</td>");

			try {
				driver.findElement(By.id("nothing"));
				PrintWriter.append("<td>Guide Info Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Guide Info" + ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Guide Info not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Guide Info" + ".jpg"));
			}

			// Manage Customer Reviews

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/ManageRatingsReviwsPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Manage Customer Reviews loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Manage Customer Reviews Succussfully loaded</td>");

			String pog = driver.findElement(By.id("programName")).getAttribute(
					"value");
			if (pog.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Manage Customer Reviews Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Manage Customer Reviews" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Manage Customer Reviews not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Manage Customer Reviews" + ".jpg"));
			}

			// Deposit Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/DepositSetupPage.do?module=marketing");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Deposit Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Deposit Setup Succussfully loaded</td>");

			String pay = driver.findElement(By.id("paymentPercentage1"))
					.getAttribute("value");
			if (pay.equalsIgnoreCase("100")) {
				PrintWriter
						.append("<td>Deposit Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Deposit Setup"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Deposit Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Deposit Setup"
								+ ".jpg"));
			}

			// Affiliate Commission Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=51&reportName=Affiliate%20Commission%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Commission Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Affiliate Commission Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String aff = driver.findElement(By.id("affiliatename"))
					.getAttribute("value");
			if (aff.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Affiliate Commission Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Affiliate Commission Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Affiliate Commission Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Affiliate Commission Report" + ".jpg"));
			}

			// Affiliate Summary Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=53&reportName=Affiliate%20Summary%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Summary Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Affiliate Summary Report Succussfully loaded </td>");

			driver.switchTo().frame("reportIframe");
			String affliate = driver.findElement(By.id("affiliatename"))
					.getAttribute("value");
			if (affliate.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Affiliate Summary Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Affiliate Summary Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Affiliate Summary Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Affiliate Summary Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Affiliate Booking Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=49&reportName=Affiliate%20Booking%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Booking Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Affiliate Booking Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String affBooking = driver.findElement(By.id("reportto_Year_ID"))
					.getAttribute("value");
			if (affBooking.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Affiliate Booking Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Affiliate Booking Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Affiliate Booking Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Affiliate Booking Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Affiliate Detail Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=54&reportName=Affiliate%20Detail%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Affiliate Detail Report  loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Affiliate Detail Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String affname = driver.findElement(By.id("affiliatename"))
					.getAttribute("value");
			if (affname.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Affiliate Detail Report Succussfully  loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Affiliate Detail Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Affiliate Detail Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Affiliate Detail Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Agent Details Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=24&reportName=Agent%20Details%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Agent Details Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Agent Details Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String cityIn = driver.findElement(By.id("city_inventory"))
					.getAttribute("value");
			if (cityIn.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Agent Details Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Agent Details Report"
								+ ".jpg"));

			} else {
				PrintWriter.append("<td>Agent Details Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Agent Details Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Agent Performance Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=4&reportName=Agent%20Performance%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Agent Performance Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Agent Performance Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String agent = driver.findElement(By.id("consultName"))
					.getAttribute("value");
			if (agent.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Agent Performance Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Agent Performance Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Agent Performance Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Agent Performance Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Customer Profile View
			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/marketting/CustomerProfileViewAction.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Customer Profile View loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Customer Profile View Succussfully loaded</td>");

			try {
				driver.findElement(By.id("firstName"));
				PrintWriter
						.append("<td>Customer Profile View Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Customer Profile View"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Customer Profile View not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Customer Profile View"
								+ ".jpg"));
			}

			// Mailing List

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/marketting/MailingListAction.do?module=marketing");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Mailing List loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Mailing List Succussfully loaded</td>");

			try {
				driver.findElement(By.id("reportName"));
				PrintWriter.append("<td>Mailing List Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Mailing List"
										+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Mailing List not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Mailing List"
										+ ".jpg"));
			}

			// Travel Agent Commission

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/operational/travelAgentCommissionReport.do?module=marketing");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Travel Agent Commission loaded/td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Travel Agent Commission not loaded</td>");

			/*
			 * driver.switchTo().frame(""); String
			 * =driver.findElement(By.id("")).getAttribute("value"); if( )
			 */
			try {
				driver.findElement(By.id(""));
				PrintWriter
						.append("<td>Travel Agent Commission loaded Successfully</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Travel Agent Commission" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Travel Agent Commission not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Travel Agent Commission" + ".jpg"));
			}

			// Customer Reservation History Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=44&reportName=Customer%20Reservation%20History%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Customer Reservation History Report loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Customer Reservation History Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String yrIdFm = driver.findElement(By.id("reportfrom_Year_ID"))
					.getAttribute("value");
			if (yrIdFm.equalsIgnoreCase("2015")) {
				PrintWriter
						.append("<td>Customer Reservation History Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Customer Reservation History Report"
								+ ".jpg"));

			} else {
				PrintWriter
						.append("<td>Customer Reservation History Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Customer Reservation History Report"
								+ ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Operator Performance Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=32&reportName=Operator%20Performance%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Operator Performance Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Operator Performance Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String user = driver.findElement(By.id("username")).getAttribute(
					"value");
			if (user.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Operator Performance Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Operator Performance Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Operator Performance Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Operator Performance Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// Lookup Info � Supplementary Services

			driver.get(PG_Properties.getProperty("Portal.Url.Activities")
					+ "/setup/SupplementaryServicesPage.do?module=marketing");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Supplementary Services loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Supplementary Services Succussfully loaded</td>");

			String pmup = driver.findElement(By.id("profitMarkup"))
					.getAttribute("value");
			if (pmup.equalsIgnoreCase("0.00")) {
				PrintWriter
						.append("<td>Supplementary Services Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Supplementary Services" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Supplementary Services not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Supplementary Services" + ".jpg"));
			}

			// Usage Statistic Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=29&reportName=Usage%20Statistic%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Usage Statistic Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Usage Statistic Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String origin = driver.findElement(By.id("origin")).getAttribute(
					"value");
			if (origin.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>Usage Statistic Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Usage Statistic Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>Usage Statistic Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Usage Statistic Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			// User Access Audit Report

			driver.get(PG_Properties.getProperty("Portal.Url")
					+ "/reports/operational/mainReport.do?reportId=30&reportName=User%20Access%20Audit%20Report");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check User Access Audit Report loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>User Access Audit Report Succussfully loaded</td>");

			driver.switchTo().frame("reportIframe");
			String auditReport = driver.findElement(By.id("auditusername"))
					.getAttribute("value");
			if (auditReport.equalsIgnoreCase("ALL")) {
				PrintWriter
						.append("<td>User Access Audit Report Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "User Access Audit Report" + ".jpg"));

			} else {
				PrintWriter
						.append("<td>User Access Audit Report not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "User Access Audit Report" + ".jpg"));
			}
			driver.switchTo().defaultContent();

			
			
			PrintWriter.append("</table>");
			PrintWriter.append("</body></html>");

			PrintWriter.append("<br>");

			PrintWriter
					.append("<p class='InfoSub' style='font-weight: bold;'>Admin Module</p>");

			PrintWriter
					.append("<br><table ><tr><th>TestCase#</th><th>TestDescription</th><th>Expected</th><th>Actual</th><th>Test Status</th><th>Errors</th></tr>");
			
			Tab1.TestCaseCount++;
			// Admin

			// Currency Exchange Rate Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/CurrencyExchangeRateSetupPage.do?module=admin");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Currency Exchange Rate Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>Currency Exchange Rate Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("screenname"));
				PrintWriter
						.append("<td>Currency Exchange Rate Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/"
								+ "Currency Exchange Rate" + ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>Currency Exchange Rate Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/"
								+ "Currency Exchange Rate" + ".jpg"));
			}

			// Lookup Info � Currency Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/CurrencySetupPage.do?module=admin");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Currency Setup loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Currency Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("currencyName"));
				PrintWriter
						.append("<td>Currency Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Currency Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Currency Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Currency Setup"
								+ ".jpg"));
			}

			// Lookup Info � Credit Card Type

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/CreditCardTypesPage.do?module=admin");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Credit Card Type loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Credit Card Type Succussfully loaded</td>");

			try {
				driver.findElement(By.id("creditCardFees"));
				PrintWriter
						.append("<td>Credit Card Type Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Credit Card Type"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Credit Card Type not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Credit Card Type"
								+ ".jpg"));
			}

			// POS Location Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/PosSetupStandardPage.do?module=admin");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check POS Location Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td> POS Location Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("addressline1"));
				PrintWriter
						.append("<td> POS Location Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "POS Location Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td> POS Location Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "POS Location Setup"
								+ ".jpg"));
			}

			// Region Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/RegionSetupPage.do?module=admin");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Region Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Region Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("regionName"));
				PrintWriter.append("<td>Region Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Region Setup"
										+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Region Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Region Setup"
										+ ".jpg"));
			}

			// User Password Change

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/ChangePasswordPage.do?module=admin");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check User Password Change loaded Succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter
					.append("<td>User Password Change Succussfully loaded</td>");

			try {
				driver.findElement(By.id("newPassword"));
				PrintWriter
						.append("<td>User Password Change loaded Succussfully</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "User Password Change"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter
						.append("<td>User Password Change not Succussfully loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "User Password Change"
								+ ".jpg"));
			}

			// Deep Link Setup

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/DeepLinkSetupPage.do?module=admin");

			PrintWriter
					.append("<tr><td>"
							+ Tab1.TestCaseCount
							+ "</td><td>Check Deep Link Setup loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Deep Link Setup Succussfully loaded</td>");

			try {
				driver.findElement(By.id("hotelName"));
				PrintWriter
						.append("<td>Deep Link Setup Succussfully loaded</td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("PassedScreenshots/" + "Deep Link Setup"
								+ ".jpg"));

			} catch (Exception e) {
				PrintWriter.append("<td>Deep Link Setup not loaded</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),
						new File("FailedScreenshots/" + "Deep Link Setup"
								+ ".jpg"));
			}

			// Setup Users

			driver.get(PG_Properties.getProperty("Portal.Url.Admin")
					+ "/setup/SetupUsersPage.do?module=admin");

			PrintWriter.append("<tr><td>" + Tab1.TestCaseCount
					+ "</td><td>Check Setup Users loaded succussfully</td>");
			Tab1.TestCaseCount++;
			PrintWriter.append("<td>Setup Users Succussfully loaded </td>");

			String max = driver.findElement(By.id("maxdiscount")).getAttribute(
					"value");
			if (max.equalsIgnoreCase("0")) {
				PrintWriter.append("<td>Setup Users Succussfully loaded </td>");
				PrintWriter.append("<td class='Passed'>PASS</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("PassedScreenshots/" + "Setup Users"
										+ ".jpg"));

			} else {
				PrintWriter.append("<td>Setup Users not loaded </td>");
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				PrintWriter.append("<td></td></tr>");
				TakesScreenshot screen = (TakesScreenshot) driver;
				FileUtils
						.copyFile(screen.getScreenshotAs(OutputType.FILE),
								new File("FailedScreenshots/" + "Setup Users"
										+ ".jpg"));
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		PrintWriter.append("</table>");
		PrintWriter.append("</body></html>");
	}

	@After
	public void tearDown() throws IOException {

		BufferedWriter sbwr = new BufferedWriter(new FileWriter(new File(
				"D:/Dulan/ScreenValidationScript/ScreenValidationReport.html")));
		sbwr.write(PrintWriter.toString());
		sbwr.flush();
		sbwr.close();
		PrintWriter.setLength(0);

		// driver.quit();
	}

}
